import { SEARCH_MOVIE } from '../../api'
import httpRequest from '../../api/service'

export const FETCH_SEARCH_MOVIE_BEGIN = 'FETCH_SEARCH_MOVIE_BEGIN';
export const FETCH_SEARCH_MOVIE_SUCCESS = 'FETCH_SEARCH_MOVIE_SUCCESS';
export const FETCH_SEARCH_MOVIE_FAILURE = 'FETCH_SEARCH_MOVIE_FAILURE';

export const fetchSearchMovieBegin = () => ({
    type: 'FETCH_SEARCH_MOVIE_BEGIN'
});

export const fetchSearchMovieSuccess = data => ({
    type: 'FETCH_SEARCH_MOVIE_SUCCESS',
    payload: data
});

export const fetchSearchMovieFailure = error => ({
    type: 'FETCH_SEARCH_MOVIE_FAILURE',
    payload: error
});

export const requestSearchMovie = (Movies) => {
	return dispatch => {
		dispatch(fetchSearchMovieBegin())
	  	return httpRequest.get(SEARCH_MOVIE +'s='+ Movies).then(function (response) {
	     	if (response.data.code == 200) {
	     		dispatch(fetchSearchMovieSuccess(response.data));
		      	return response.data;
	     	} else {
	     		dispatch(fetchSearchMovieFailure(response.data));
		      	return response.data;
	     	}
	  	}).catch(function (error) {
	     	dispatch(fetchSearchMovieFailure(error.response))
	  	});
	}
}