import { TODAY_HITS } from '../../api'
import httpRequest from '../../api/service'

export const FETCH_HITS_MOVIE_BEGIN = 'FETCH_HITS_MOVIE_BEGIN';
export const FETCH_HITS_MOVIE_SUCCESS = 'FETCH_HITS_MOVIE_SUCCESS';
export const FETCH_HITS_MOVIE_FAILURE = 'FETCH_HITS_MOVIE_FAILURE';

export const fetchHitsMovieBegin = () => ({
    type: 'FETCH_HITS_MOVIE_BEGIN'
});

export const fetchHitsMovieSuccess = data => ({
    type: 'FETCH_HITS_MOVIE_SUCCESS',
    payload: data
});

export const fetchHitsMovieFailure = error => ({
    type: 'FETCH_HITS_MOVIE_FAILURE',
    payload: error
});

export const requestHitsMovie = () => {
	return dispatch => {
		dispatch(fetchHitsMovieBegin())
	  	return httpRequest.get(TODAY_HITS).then(function (response) {
	     	if (response.data.code == 200) {
	     		dispatch(fetchHitsMovieSuccess(response.data));
		      	return response.data;
	     	} else {
	     		dispatch(fetchHitsMovieFailure(response.data));
		      	return response.data;
	     	}
	  	}).catch(function (error) {
	     	dispatch(fetchHitsMovieFailure(error.response))
	  	});
	}
}