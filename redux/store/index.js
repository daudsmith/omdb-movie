import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import rootReducers from '../reducer'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

const makeConfiguredStore = (reducer, initialState) => {
  return createStore(
    reducer,
    initialState,
    applyMiddleware(thunkMiddleware)
  )
}

const initialState = {};

export default (initialState, {isServer}) => {

		if (isServer) {
			initialState = initialState;
			return makeConfiguredStore(rootReducers, initialState);
		} else {
			let persistConfig = {
				key: window.location.host,
				storage,
				whitelist: ['hitsMovie']
			};
  
  

			let persistedReducer = persistReducer(persistConfig, rootReducers);
			let store = makeConfiguredStore(persistedReducer, initialState);

			store.__persistor = persistStore(store);
			return store;
		}
}