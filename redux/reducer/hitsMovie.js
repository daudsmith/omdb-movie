import {FETCH_HITS_MOVIE_BEGIN, FETCH_HITS_MOVIE_SUCCESS, FETCH_HITS_MOVIE_FAILURE} from '../action/hitsMovie'

const initialReducers = {
    result: null,
    isFetching: false, 
    error: false
}

const hitsMovieReducer = (state = initialReducers, action) => {
    switch (action.type) {
        case 'FETCH_HITS_MOVIE_BEGIN':
                state = {
						...state,
						isFetching: true, 
						error: false
				}
                break;
          case 'FETCH_HITS_MOVIE_SUCCESS':
            state = {
                    ...state,
                    result: action.payload,
                    isFetching: false, 
                    error: false
            }
                break;
          case 'FETCH_HITS_MOVIE_FAILURE':
            state = {
                    ...state,
                    result: action.payload,
                    isFetching: false, 
                    error: true
            }
              break;
        default:
            break;
    }
    return state
}

export default hitsMovieReducer;