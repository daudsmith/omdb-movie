import {FETCH_SEARCH_MOVIE_BEGIN, FETCH_SEARCH_MOVIE_SUCCESS, FETCH_SEARCH_MOVIE_FAILURE} from '../action/searchMovie'

const initialReducers = {
    isFetching: false,
	isError: false,
	data: null

}

const searchMovieReducer = (state = initialReducers, action) => {
    switch (action.type) {
        case 'FETCH_SEARCH_MOVIE_BEGIN':
                state = {
						...state,
						isFetching: true, 
						error: false
				}
                break;
          case 'FETCH_SEARCH_MOVIE_SUCCESS':
            state = {
                    ...state,
                    data: action.payload,
                    isFetching: false, 
                    error: false
            }
                break;
          case 'FETCH_SEARCH_MOVIE_FAILURE':
            state = {
                    ...state,
                    data: action.payload,
                    isFetching: false, 
                    error: true
            }
              break;
        default:
            break;
    }
    return state
}

export default searchMovieReducer;