import { combineReducers } from 'redux';
import { loadingBarReducer } from 'react-redux-loading-bar';
import hitsMovie from './hitsMovie';
import searchMovie from './searchMovie';


export default combineReducers({
	loadingBar: loadingBarReducer,
	hitsMovie: hitsMovie,
	searchMovie: searchMovie
});