import React from 'react'
import MainPage from '../../../components/mainpages'

class mainpages extends React.Component {
 
    render() {
       return (
         <div className="maincontent">
            <MainPage/>
            <style jsx global>
            {`  
                .cardresult {
                  height: 40px;
                  margin-top: 60px;
                }
                .buttoninput {
                  margin-top: 20px;
                  margin-right: 15px;
                }
                .forminput {
                  margin-top: 20px;
                  margin-right: 15px;
                }
                .searchtitle {
                  color: #ecf0f1;
                  font-weight: bold;
                  font-size: 20px;
                  margin-top: 25px;
                }
                .navsearch {
                    margin-top: 90px;
                    height: 80px;
                    box-shadow: 1px 1px 6px #ecf0f1;
                    background: #c0392b;
                }
                .movieshits {
                    border-radius: 4px;
                    margin: 20px;
                    background-color: #1b1b1b;
                    height: 300px;
                    justify-content: center;
                    box-shadow: 1px 1px 4px rgba(129, 129, 129, 0.785);
                  }
                  .cardhits {
                    margin-top: 80px;
                  }
                  .favouritetitle {
                    color: #ecf0f1;
                    font-weight: bold;
                    font-size: 25px;
                    margin-top: 90px;
                    margin-bottom: 20px;
                  }
                  .jumbotron {
                    background-color: #1b1b1b;
                  }
                  .jumbotrontitle {
                    color: #ecf0f1;
                    font-weight: bold;
                    font-size: 55px;
                  }
                  .jumbotronsubtitle {
                    color: #ecf0f1;
                    font-size: 15px;
                    margin-bottom: 20px;
                  }
                  .favourites {
                     margin-top: -30px;
                  }
                  .cardchoice {
                    background-color: #1b1b1b;
                    box-shadow: 1px 1px 4px rgba(129, 129, 129, 0.785);
                    border-radius: 8px;
                  }
                  .cardfavourites {
                    background-color: #1b1b1b;
                    box-shadow: 1px 1px 4px rgba(129, 129, 129, 0.785);
                    border-radius: 8px;
                    height: 240px
                  }
                  .maincontent {
                    background-color: #2d3436;
                    margin-top: 80px;
                  }
                  
            `}
            </style>
         </div>
       );
    }
 }
 
 export default (mainpages)