import React from 'react';
import { Navbar } from 'reactstrap';
import NavBar from '../../../components/mainpages/navbar'

class Navigation extends React.Component {

  render() {
    return (
      <Navbar className="navigationbar" fixed="top">
        <NavBar/>
        <style jsx global>
            {`
            .sitetitle{
              color: #ecf0f1;
              font-weight: bold;
            }
            .navigationbar {
              height: 80px;
              box-shadow: 1px 1px 6px #2c3e50;
              background: #c0392b;
            }    
            `}
            </style>
      </Navbar>
    );
 }
}

export default Navigation;