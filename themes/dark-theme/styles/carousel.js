import React from 'react';
import { UncontrolledCarousel } from 'reactstrap';

const items = [
  {
    src: './assets/images/last-samurai.png',
    altText: 'The Last Samurai',
    caption: 'Lorem Ipsum Dollor Sit Ammet',
    header: 'The Last Samurai',
    key: '1'
  },
  {
    src: './assets/images/fast-forious.png',
    altText: 'Fast and Forious',
    caption: 'Lorem Ipsum Dolor Sit Ammet',
    header: 'Fast 9',
    key: '2'
  },
  {
    src: './assets/images/godzila-kong.png',
    altText: 'Godzila VS Kong',
    caption: 'Lorem Ipsum Dollor Sit Ammet',
    header: 'Godzila VS Kong',
    key: '3'
  }
];

const Carousel = () => 
    <div className="carousel-style">
        <UncontrolledCarousel items={items} />
    <style jsx global>
        {`
        .carousel-style{
            width: 100%;
            height: 90%;
            margin-top: 80px;
            box-shadow: 1px 1px 6px #636e72;
        }
        `}
    </style>
</div>

export default Carousel;