import React from 'react'
import Navbar from './navbar'
import Carousel from './carousel'
import MainPage from './mainpage'
import Footer from './footer'

class Index extends React.Component {
 
    render() {
       return (
         <div className="homestyle">
               <Navbar/>
               <Carousel/>
               <MainPage/>
               <Footer/>
         <style jsx global>
         {`
         .homestyle{
            background-color: #2d3436;
            margin-top: -20px
         }
         @media (max-width: 600px) {
            .grid {
               width: 100%;
               flex-direction: column;
            }
         }   
         `}
         </style>
         </div>
       );
    }
 }
 
 export default (Index)

