const express = require('express')
const next = require('next')

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()
const bodyParser = require('body-parser')
const request = require('request')

app.prepare()
.then(() => {
  const server = express()
  server.use(bodyParser.urlencoded({ extended: false }));
  server.set('views', __dirname + '/views'); // set express to look in this folder to render our view
  server.set('view engine', 'ejs'); // configure template engine
  
  server.use(function(req ,res , next){
    res.set('X-XSS-Protection', '1;mode=block');
    res.set('X-Content-Type-Options', 'nosniff');
    res.set('X-Frame-Options', 'sameorigin');
    res.set('Strict-Transport-Security', 'max-age=31536000; includeSubDomains');
    next();
  })

  server.get('*', (req, res) => {
    return handle(req, res)
  })

  server.listen(process.env.PORT, (err) => {
    if (err) throw err
    console.log('Ready on http://localhost:'+process.env.PORT)
  })
})
.catch((ex) => {
  console.error(ex.stack)
  process.exit(1)
})