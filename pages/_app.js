import React from 'react';
import App from 'next/app';
import withRedux from 'next-redux-wrapper'
import initStore from '../redux/store'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Provider, connect } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'

class MyApp extends App {

  render() {
    const { Component, pageProps, store  } = this.props;
    
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={store.__persistor}>
            <Component {...pageProps} />
        </PersistGate>
      </Provider>
    );
  }
}


const mapStateToProps = (state) => {
  return {
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
  }
}


export default withRedux(initStore, { debug: false })(
  connect(mapStateToProps, mapDispatchToProps)(MyApp)
)