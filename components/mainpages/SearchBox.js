import React from 'react'
import { connect } from 'react-redux'
import { requestSearchMovie } from '../../redux/action/searchMovie'
import { withRouter } from 'next/router'
import { Button, Row, Col, Input, FormText, Spinner } from 'reactstrap';
import SearchResult from './SearchResult'

class SearchBox extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
           Movies: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
  
     handleChange(e) {
        let self = this;
        self.setState({
           Movies: e.target.value
        });
     }
  
     handleSubmit() {
        let Movies = this.state.Movies;
        if (this.state.Movies) {
           this.props.requestSearchMovie(Movies);
        } else {
           this.setState({ errorMessage: 'Please insert movies title' })
        }
     }
 
    render() {

    const { isFetching, isError, data } = this.props.searchMovie;

       return (
           <>
		   <div className="navsearch">
			<Col xs="12">
				<Row>
					<Col xs="8">
						<div className="searchtitle">Semua Film Menarik Ada Disini.</div>
						</Col>
						<Col xs="4">
							<Row>
								<div className="forminput">
								<Input type="text"
									name="value"
									value={this.state.Movies}
									placeholder="Cari Film Anda"
									onChange={this.handleChange} />
									<FormText>{this.state.errorMessage}</FormText>
								</div>
								<div className="buttoninput">
									<Button onClick={this.handleSubmit} color="primary" block>Cari Film</Button>
								</div>
							</Row>
						</Col>
					</Row>
				</Col>
            </div>
			<div className="favourites">
               <Col xs="12">
                     <div className="cardresult">
						{
							isFetching ? (
								<center><Spinner/></center>
							) : (
								<SearchResult data={data}/>
							)
						}
                     </div>
               </Col>
            </div>
            </>
       );
    }
 }
 
const mapStateToProps = (state) => {
    return {
       searchMovie: state.searchMovie
    }
 }
 
 const mapDispatchToProps = (dispatch) => {
    return {
       requestSearchMovie: (param) => dispatch(requestSearchMovie(param))
    }
 }

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SearchBox))