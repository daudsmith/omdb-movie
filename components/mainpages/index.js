import React from 'react'
import { connect } from 'react-redux'
import { requestHitsMovie } from '../../redux/action/hitsMovie'
import { withRouter } from 'next/router'
import { Button, Row, Col, Card, Jumbotron } from 'reactstrap';
import Favourites from '../favourites'
import SearchBox from './SearchBox'

class Index extends React.Component {

   constructor(props) {
      super(props);
      this.state = {
         
      };
    }

    UNSAFE_componentWillMount() {
      this.props.requestHitsMovie();
   }
    
    render() {
      let data = this.props.hitsMovie.result;

       return (
           <>
            <Jumbotron className="jumbotron">
               <div className="jumbotrontitle">DAPATKAN DISKON 30%</div>
               <div className="jumbotronsubtitle">Dapatkan Diskon mulai dari 30% dengan menginstall aplikasi Bibit.</div>
               <Button color="danger">Coba Sekarang</Button>
            </Jumbotron>
            <div className="favourites">
               <SearchBox/>
            </div>
            <div className="favourites">
               <Col xs="12">
               <div className="favouritetitle">Yang Sedang Hits</div>
                  <Row>
                     { data && data.Search.map ((hits, i) => {
                     return <Col md={3} xs={12} className="cardhits" key={i}>
                              <Card className="movieshits">
                                 <img src={hits.Poster} alt='movie'></img>
                              </Card>
                           </Col>
                           }
                        )
                     }
                  </Row>
               </Col>
            </div>
            <div className="favourites">
               <Favourites/>
            </div>
            </>
       );
    }
 }
 
 
const mapStateToProps = (state) => {
   return {
      hitsMovie: state.hitsMovie
   }
}

const mapDispatchToProps = (dispatch) => {
   return {
      requestHitsMovie: () => dispatch(requestHitsMovie())
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Index))