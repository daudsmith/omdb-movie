import React from 'react';
import { connect } from 'react-redux'
import { withRouter } from 'next/router'
import { NavbarBrand } from 'reactstrap';

class Navigation extends React.Component {


    render() {

    return (
        <>
            <NavbarBrand href="/" className="mr-auto">
                <div className="sitetitle">OMDB Movies</div>
            </NavbarBrand>
        </>
    );
  }
}
 
 
const mapStateToProps = (state) => {
   return {
   }
}

const mapDispatchToProps = (dispatch) => {
   return {
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Navigation))