import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'next/router'
import { Card, CardBody, Row, Col } from 'reactstrap';

class SearchResult extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        };
   }
 
    render() {

    const { data } = this.props.searchMovie;
    let films = null;

    if (data != null) {
       films = data;
      if (films) {
          return (
            <Row>
            { data && data.Search.map ((search, i) => {
            return <Col md={3} xs={12} className="cardhits" key={i}>
                     <Card className="movieshits" key={i}>
                        <img src={search.Poster} alt='movie'></img>
                     </Card>
                  </Col>
                  }
               )
            }
         </Row>
          )
      } else {
          return (
          <div>
               <h4>Opps.. Movies Not Found</h4>
          </div>)
      }
      } else {
            return (
               <Card className="cardfavourites">
                    <h4>Nothing Movies Here, Please Search...</h4>
               </Card>)
      }

    }
 }
 
const mapStateToProps = (state) => {
    return {
       searchMovie: state.searchMovie
    }
 }
 
 const mapDispatchToProps = (dispatch) => {
    return {
    }
 }

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SearchResult))